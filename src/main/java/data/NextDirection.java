package data;

import org.json.JSONException;
import org.json.JSONObject;

public class NextDirection {
    private double direction;
    private double distance;
    private Location lastLocation;

    public NextDirection(double direction, double distance, Location lastLocation) {
        this.direction = direction;
        this.distance = distance;
        this.lastLocation = lastLocation;
    }

    public double getDirection() {
        return direction;
    }
    public void setDirection(double direction) {
        this.direction = direction;
    }

    public double getDistance() {
        return distance;
    }
    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Location getLastLocation() { return lastLocation; }
    public void setLastLocation(Location lastLocation) { this.lastLocation = lastLocation; }

    @Override
    public String toString() {
        return "NextDirection{" +
                "direction=" + direction +
                ", distance=" + distance +
                ", lastLocation=" + lastLocation.getName() +
                '}';
    }

    public String toJson() throws JSONException {
        return new JSONObject()
                .put("direction", direction)
                .put("distance", distance)
                .put("lastLocation", lastLocation.getName())
                .toString();
    }
}
