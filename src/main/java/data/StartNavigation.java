package data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StartNavigation {
    private double initialPositionX;
    private double initialPositionY;
    private int initialFloor;
    private int destination;
    private boolean isReducedMobilityFriendly;

    public StartNavigation(){}

    public StartNavigation(double initialPositionX, double initialPositionY, int initialFloor, int destination, boolean isReducedMobilityFriendly) {
        this.initialPositionX = initialPositionX;
        this.initialPositionY = initialPositionY;
        this.initialFloor = initialFloor;
        this.destination = destination;
        this.isReducedMobilityFriendly = isReducedMobilityFriendly;
    }

    public double getInitialPositionX() {
        return initialPositionX;
    }
    public void setInitialPositionX(double initialPositionX) {
        this.initialPositionX = initialPositionX;
    }

    public double getInitialPositionY() {
        return initialPositionY;
    }
    public void setInitialPositionY(double initialPositionY) {
        this.initialPositionY = initialPositionY;
    }

    public int getInitialFloor() {
        return initialFloor;
    }
    public void setInitialFloor(int initialFloor) {
        this.initialFloor = initialFloor;
    }

    public int getDestination() {
        return destination;
    }
    public void setDestination(int destination) {
        this.destination = destination;
    }

    public boolean isReducedMobilityFriendly() {
        return isReducedMobilityFriendly;
    }
    public void setReducedMobilityFriendly(boolean reducedMobilityFriendly) {
        isReducedMobilityFriendly = reducedMobilityFriendly;
    }

    @Override
    public String toString() {
        return "StartNavigation{" +
                "initialPositionX=" + initialPositionX +
                ", initialPositionY=" + initialPositionY +
                ", initialFloor=" + initialFloor +
                ", destination=" + destination +
                ", isReducedMobilityFriendly=" + isReducedMobilityFriendly +
                '}';
    }

    /*
        {
            "initial_position": [
                42.154,
                58.648
            ],
            "initial_floor": 2,
            "destination": 10,
            "isReducedMobilityFriendly": true
        }
        */
    public void fromJson(String jsonString) throws JSONException {
        JSONObject object = new JSONObject(jsonString);

        JSONArray initialPosition = object.getJSONArray("initial_position");
        this.setInitialPositionX(initialPosition.getDouble(0));
        this.setInitialPositionY(initialPosition.getDouble(1));

        this.setInitialFloor(object.getInt("initial_floor"));
        this.setDestination(object.getInt("destination"));
        this.setReducedMobilityFriendly(object.getBoolean("isReducedMobilityFriendly"));
    }
}
