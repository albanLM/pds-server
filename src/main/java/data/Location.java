package data;

import org.json.JSONException;
import org.json.JSONObject;

public class Location{
    private int id;
    private String name;
    private Integer floor;
    private String site;
    private double latitude;
    private double longitude;
    private boolean isReducedMobilityFriendly;
    private String nodeType;

    public Location(){}

    public Location(int id, String name, Integer floor, String site, double latitude, double longitude, boolean isReducedMobilityFriendly, String nodeType) {
        this.id = id;
        this.name = name;
        this.floor = floor;
        this.site = site;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isReducedMobilityFriendly = isReducedMobilityFriendly;
        this.nodeType = nodeType;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getFloor() {
        return floor;
    }
    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getNodeType() {
        return nodeType;
    }
    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public boolean isReducedMobilityFriendly() {
        return isReducedMobilityFriendly;
    }
    public void setReducedMobilityFriendly(boolean reducedMobilityFriendly) {
        isReducedMobilityFriendly = reducedMobilityFriendly;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", floor=" + floor +
                ", site='" + site + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", isReducedMobilityFriendly=" + isReducedMobilityFriendly +
                ", nodeType='" + nodeType + '\'' +
                '}';
    }

    /*
    {
        "id": 10,
        "name": "Balcon Cote Rue"
    }
    */
    public String toJson() throws JSONException {
        return new JSONObject()
                .put("id", this.getId())
                .put("name", this.getName())
                .toString();
    }
}