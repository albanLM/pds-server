package data;

import javax.vecmath.Vector2d;

public class Link{
    private int id;
    private double distance;
    private double orientation;
    private Vector2d vector;
    private int nodeId1;
    private int nodeId2;

    public Link(){}

    public Link(int id, double distance, double orientation, int nodeId1, int nodeId2) {
        this.id = id;

        System.out.println("Inputs - orientation : " + orientation + ", distance : " + distance);

        // Calcule le vecteur de déplacement à effectuer entre les deux positions
        // Cos et Sin prennent des angles en radians
        // Nécessite l'inversion des valeurs car les radians sont mesurés dans le sens horaire et l'orientation dans le sens anti-horaire
        double x = -Math.sin(Math.toRadians(-(orientation))) * distance;
        double y = Math.cos(Math.toRadians(-(orientation))) * distance;
        vector = new Vector2d(x, y);

        // Calcule l'angle entre la direction que doit prendre l'utilisateur et
        this.orientation = vector.angle(new Vector2d(0, 1)) * Math.signum(-vector.getX());
        this.distance = vector.length();

        this.nodeId1 = nodeId1;
        this.nodeId2 = nodeId2;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }
    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getOrientation() {
        return orientation;
    }
    public void setOrientation(double orientation) {
        this.orientation = orientation;
    }

    public int getNodeId1() {
        return nodeId1;
    }
    public void setNodeId1(int nodeId1) {
        this.nodeId1 = nodeId1;
    }

    public int getNodeId2() {
        return nodeId2;
    }
    public void setNodeId2(int nodeId2) {
        this.nodeId2 = nodeId2;
    }

    public Vector2d getVector() {
        return vector;
    }

    public void setVector(Vector2d vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
        return "Link{" +
                "id=" + id +
                ", distance=" + distance +
                ", orientation=" + orientation +
                ", nodeId1=" + nodeId1 +
                ", nodeId2=" + nodeId2 +
                '}';
    }
}