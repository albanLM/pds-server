package data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.vecmath.Vector2d;

public class SensorsVectors {
    private Vector2d v;

    public SensorsVectors(){}

    public Vector2d getV() {
        return v;
    }
    public void setV(Vector2d v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "SensorsVectors{" +
                "v=" + v +
                '}';
    }

    /*
        {
            "vector": [
                0.25,
                0.65,
                1.21
            ]
        }
        */
    public void fromJson(String jsonString) throws JSONException {
        JSONObject object = new JSONObject(jsonString);

        JSONArray v = object.getJSONArray("vector");
        double x = v.getDouble(0);
        double y = v.getDouble(1);

        Vector2d vector = new Vector2d(x, y);
        this.setV(vector);
    }
}