package database;

import data.Link;
import data.Location;
import org.neo4j.driver.*;
import org.neo4j.driver.exceptions.AuthenticationException;
import org.neo4j.driver.exceptions.ServiceUnavailableException;
import org.neo4j.driver.exceptions.UntrustedServerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Queries{
    private Driver driver;
    public ArrayList<Link> pathLinks = new ArrayList<>();
    public ArrayList<Location> pathLocations = new ArrayList<>();
    public Link currentLink = null;

    public Queries() throws AuthenticationException, ServiceUnavailableException, UntrustedServerException {
        this.setDriver(Neo4JConnection.getDriver());
    }

    public Driver getDriver() {
        return driver;
    }
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Location getClosestLocation(double latitude, double longitude){
        String query = "MATCH (a)\n" +
                "WITH toFloat(a.latitude)-$latitude + toFloat(a.longitude)-$longitude as latlong, a \n" +
                "WHERE a.ReducedMobilityFriendly IN [\"False\",\"True\"]\n" +
                "RETURN ID(a), a.location_name, a.location_floor, a.site_name, a.latitude, a.longitude, a.ReducedMobilityFriendly, labels(a) as labels\n" +
                "ORDER BY latlong ASC\n" +
                "LIMIT 1;";

        Query q = new Query(query);

        Map<String, Object> params = new HashMap<>();
        params.put("latitude", latitude);
        params.put("longitude", longitude);

        Location location = new Location();

        try(Session session = this.getDriver().session()){
            return session.readTransaction(tx -> {
                Result result = tx.run(q.withParameters(params));
                while(result.hasNext()){
                    Record current = result.next();
                    location.setId(current.get(0).asInt());
                    location.setName(current.get(1).asString());
                    location.setFloor(Integer.valueOf(current.get(2).asString()));
                    location.setSite(current.get(3).asString());
                    location.setLatitude(Double.parseDouble(current.get(4).asString()));
                    location.setLongitude(Double.parseDouble(current.get(5).asString()));
                    location.setReducedMobilityFriendly(Boolean.parseBoolean(current.get(6).asString()));
                    location.setNodeType(current.get(7).asList().get(0).toString());
                }
                return location;
            });
        }
    }

    public ArrayList<Location> getTerminalLocations(){
        String query = "MATCH (a:terminal_node)\n" +
                        "WHERE a.ReducedMobilityFriendly IN [\"False\",\"True\"]\n" +
                        "RETURN ID(a), a.location_name, a.location_floor, a.site_name, a.latitude, a.longitude, a.ReducedMobilityFriendly, labels(a) as labels\n;";
        Query q = new Query(query);

        ArrayList<Location> locations = new ArrayList<>();

        try(Session session = this.getDriver().session()){
            return session.readTransaction(tx -> {
                Result result = tx.run(q);
                while(result.hasNext()){
                    Record current = result.next();

                    Location location = new Location();
                    location.setId(current.get(0).asInt());
                    location.setName(current.get(1).asString());
                    location.setFloor(Integer.valueOf(current.get(2).asString()));
                    location.setSite(current.get(3).asString());
                    location.setLatitude(Double.parseDouble(current.get(4).asString()));
                    location.setLongitude(Double.parseDouble(current.get(5).asString()));
                    location.setReducedMobilityFriendly(Boolean.parseBoolean(current.get(6).asString()));
                    location.setNodeType(current.get(7).asList().get(0).toString());

                    locations.add(location);
                }
                return locations;
            });
        }
    }

    public Integer createShortestPath(Location startLocation, int endLocationId, boolean isReducedMobilityFriendly){
        String query = "MATCH c=shortestPath((a)-[:LEADS_TO*]->(b:terminal_node) )\n" +
                        "WHERE ID(a) = $startNode\n" +
                        "AND ID(b) = $endNode\n";
        if(isReducedMobilityFriendly){
            query += "AND a.ReducedMobilityFriendly =\"True\"\n" +
                    "AND b.ReducedMobilityFriendly = \"True\"\n" +
                    "AND ALL(x IN nodes(c)[1..-1] WHERE x.ReducedMobilityFriendly = \"True\")\n";
        }
        query += "WITH DISTINCT(relationships(c)) as e,c\n" +
                "WITH DISTINCT(nodes(c)) as f,e\n" +
                "UNWIND (e+f) as g\n" +
                "RETURN g;";
        Query q = new Query(query);

        Map<String, Object> params = new HashMap<>();
        params.put("startNode", startLocation.getId());
        params.put("endNode", endLocationId);

        pathLinks.clear();
        pathLocations.clear();

        try(Session session = this.getDriver().session()){
            return session.readTransaction(tx -> {
                Result result = tx.run(q.withParameters(params));

                while(result.hasNext()){
                    Record current = result.next();

                    if(current.get(0).type().name().equals("RELATIONSHIP")){
                        int id = (int) current.get(0).asRelationship().id();
                        double distance = Double.parseDouble(current.get(0).asRelationship().asMap().get("distance").toString());
                        double orientation = Double.parseDouble(current.get(0).asRelationship().asMap().get("orientation").toString());
                        int nodeId1 = (int) current.get(0).asRelationship().startNodeId();
                        int nodeId2 = (int) current.get(0).asRelationship().endNodeId();

                        pathLinks.add(new Link(id, distance, orientation, nodeId1, nodeId2));
                    }
                    else{
                        int id = (int) current.get(0).asNode().id();
                        String location_name = current.get(0).asNode().asMap().get("location_name").toString();
                        Integer location_floor = Integer.valueOf(current.get(0).asNode().asMap().get("location_floor").toString());
                        String site_name = current.get(0).asNode().asMap().get("site_name").toString();
                        double latitude = Double.parseDouble(current.get(0).asNode().asMap().get("latitude").toString());
                        double longitude = Double.parseDouble(current.get(0).asNode().asMap().get("longitude").toString());
                        boolean reducedMobilityFriendly = Boolean.parseBoolean(current.get(0).asNode().asMap().get("ReducedMobilityFriendly").toString());
                        String nodeType = current.get(0).asNode().labels().toString().replaceAll("[\\[\\]]", "");

                        pathLocations.add(new Location(id, location_name, location_floor, site_name, latitude, longitude, reducedMobilityFriendly, nodeType));
                    }
                }
                return 1;
            });
        }
    }
}