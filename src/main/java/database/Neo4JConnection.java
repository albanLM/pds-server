package database;

import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.exceptions.AuthenticationException;
import org.neo4j.driver.exceptions.ServiceUnavailableException;
import org.neo4j.driver.exceptions.UntrustedServerException;

public class Neo4JConnection {
    private static String uri = "bolt://86.195.2.68:7687/neo4j";
    private static String user = "neo4j";
    private static String password = "PDS";
    private static Driver driver = null;

    public static Driver getDriver() throws AuthenticationException, ServiceUnavailableException, UntrustedServerException {
        if(driver == null){
            driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
        }
        return driver;
    }

    public static String getUri() {
        return uri;
    }
    public static void setUri(String uri) {
        Neo4JConnection.uri = uri;
    }

    public static String getUser() {
        return user;
    }
    public static void setUser(String user) {
        Neo4JConnection.user = user;
    }

    public static String getPassword() {
        return password;
    }
    public static void setPassword(String password) {
        Neo4JConnection.password = password;
    }
}