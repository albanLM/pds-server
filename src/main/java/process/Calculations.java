package process;

import data.Location;
import data.NextDirection;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.awt.geom.Arc2D;

public class Calculations {
    final static Vector2d front = new Vector2d(0, 1);
    public static final double arrivalThreshold = 0.3;

    public static boolean arrivedAtNextPoint(Vector2d currentLocation, Vector2d nextLocation){
        Vector2d subtractionVector = new Vector2d();
        subtractionVector.sub(nextLocation, currentLocation);
        double norm = subtractionVector.length();

        return norm <= arrivalThreshold;
    }

    public static NextDirection newDirection(Vector2d userMovement, Vector2d nextLocation, Location lastLocation){
        Vector2d subtractionVector = new Vector2d();
        subtractionVector.sub(nextLocation, userMovement);

        // Calcule l'angle entre la direction que doit prendre l'utilisateur et
        double direction = subtractionVector.angle(front) * Math.signum(-subtractionVector.getX());
        double distance = subtractionVector.length();

        return new NextDirection(direction, distance, lastLocation);
    }
}