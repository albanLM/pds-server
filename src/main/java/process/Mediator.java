package process;

import data.*;
import database.Queries;
import org.json.JSONException;
import org.neo4j.driver.exceptions.AuthenticationException;
import org.neo4j.driver.exceptions.ServiceUnavailableException;
import org.neo4j.driver.exceptions.UntrustedServerException;

import javax.vecmath.Vector2d;
import java.util.ArrayList;
import java.util.Iterator;

public class Mediator {
    private Queries queries;
    private Iterator<Link> linkIterator;
    private Iterator<Location> locationIterator;
    private Location lastLocation;
    private Vector2d userMovement;

    public Mediator() throws AuthenticationException, ServiceUnavailableException, UntrustedServerException {
        queries = new Queries();
        userMovement = new Vector2d(0, 0);
    }

    public Queries getQueries() {
        return queries;
    }
    public void setQueries(Queries queries) {
        this.queries = queries;
    }

    public Iterator<Link> getLinkIterator() {
        return linkIterator;
    }
    public void setLinkIterator(Iterator<Link> linkIterator) {
        this.linkIterator = linkIterator;
    }

    public String getAllTerminalLocations() throws JSONException {
        ArrayList<Location> locations = queries.getTerminalLocations();

        StringBuilder jsonLocations = new StringBuilder("{\n\t\"nodes\":[\n");
        for(int i=0 ; i<locations.size() ; i++) {
            jsonLocations.append("\t\t").append(locations.get(i).toJson());

            if(i != locations.size()-1){
                jsonLocations.append(",\n");
            }
        }
        jsonLocations.append("\n\t]\n}\n");

        return jsonLocations.toString();
    }

    public boolean startNavigation(String jsonString) throws JSONException {
        StartNavigation sn = new StartNavigation();
        sn.fromJson(jsonString);

        Location initialPosition = queries.getClosestLocation(sn.getInitialPositionX(), sn.getInitialPositionY());

        queries.createShortestPath(initialPosition, sn.getDestination(), sn.isReducedMobilityFriendly());
        //nodes and relationships are in q.pathLocations and q.pathLinks

        // Retourne faux si aucun chemin n'a été trouvé
        if(queries.pathLinks.isEmpty()) return false;

        linkIterator = queries.pathLinks.iterator();
        locationIterator = queries.pathLocations.iterator();
        lastLocation = initialPosition;
        return true;
    }

    public String getNextLocation(String jsonString) throws JSONException {
        SensorsVectors sv = new SensorsVectors();
        sv.fromJson(jsonString);
        userMovement.add(sv.getV());

        Link currentLink = queries.currentLink;
        Vector2d nextLocation = currentLink.getVector();
        boolean isArrived = Calculations.arrivedAtNextPoint(userMovement, nextLocation);

        NextDirection nd = Calculations.newDirection(userMovement, nextLocation, lastLocation);
        String jsonND = nd.toJson();

        if(isArrived) {
            userMovement.sub(nextLocation, userMovement);
            if(linkIterator.hasNext()) {
                Link current = linkIterator.next();
                queries.currentLink = current;
                lastLocation = locationIterator.next();

                nd = new NextDirection(current.getOrientation(), current.getDistance(), lastLocation);
                jsonND = nd.toJson();
            } else {
                jsonND = "END";//If there is no more location
            }
        }

        return jsonND;
    }

    // Appelé au début de la navigation, quand on a pas encore d'inputs de l'utilisateur
    public String getNextLocation() throws JSONException {
        String jsonND;
        if(linkIterator.hasNext()) {
            // Récupère le premier chemin que l'utilisateur doit emprunter
            Link current = linkIterator.next();
            queries.currentLink = current;
            // Récupère le lieu de départ de l'utilisateur
            lastLocation = locationIterator.next();

            // Crée un nouvelle direction avec l'orientation et la distance du premier chemin
            NextDirection nd = new NextDirection(current.getOrientation(), current.getDistance(), lastLocation);
            jsonND = nd.toJson();
        } else {
            jsonND = "END";//If there is no more location
        }

        return jsonND;
    }

    public void closeDriver(){
        this.getQueries().getDriver().close();
    }
}