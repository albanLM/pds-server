package websockets;

import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;
import process.Mediator;

import java.util.logging.Logger;

enum Action {sendDestinations, validatePath, startNavigation, updateNavigation}

@ServerEndpoint(value = "/") // Chemin d'accès de ce point terminal
public class NavigationEndpoint {
    Action _nextAction;

    // Logs spécifiques à ce point
    public final Logger logger = Logger.getLogger(this.getClass().getName());

    // À l'ouverture d'une session, crée un mediator pour la session
    @OnOpen
    public void onOpen(Session session) {
        _nextAction = Action.sendDestinations;
        session.getUserProperties().put("mediator", new Mediator());
        logger.info("Connected ... " + session.getId());
    }

    // À la réception d'un message, lance la navigation et renvoie les infos pour le premier noeud
    @OnMessage
    public String onMessage(String message, Session session) {
//        logger.info("Message received : " + message);
        // Récupère le médiator de la session
        Mediator mediator = (Mediator) session.getUserProperties().get("mediator");
        // Exécute l'action demandée
        String response = getResponse(_nextAction, message, mediator);
        _nextAction = updateState(_nextAction, response);
        logger.info("Response sent : " + response);
        return response;
    }

    // À la fermeture de la session, affiche un log avec la cause de la fermeture
    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        Mediator mediator = (Mediator) session.getUserProperties().get("mediator");
//        mediator.closeDriver();
        logger.info(String.format("Session %s closed because of %s", session.getId(), closeReason));
    }

    String getResponse(Action actualAction, String message, Mediator mediator) {
        String response = "";
        logger.info("Actual action : " + actualAction.toString());
        switch(actualAction){
            case sendDestinations:
                response = mediator.getAllTerminalLocations();
                break;
            case validatePath:
                if(mediator.startNavigation(message))
                    response = "PATH FOUND";
                else
                    response = "NO PATH";
                break;
            case startNavigation:
                response = mediator.getNextLocation();
                break;
            case updateNavigation:
                response = mediator.getNextLocation(message);
                break;
        }
        return response;
    }

    Action updateState(Action actualAction, String response) {
        Action nextAction = actualAction;
        switch (actualAction) {
            case sendDestinations:
                nextAction = Action.validatePath;
                break;
            case validatePath:
                if(response.equals("NO PATH"))
                    nextAction = Action.sendDestinations;
                else
                    nextAction = Action.startNavigation;
                break;
            case startNavigation:
            case updateNavigation:
                nextAction = Action.updateNavigation;
                break;
        }
        logger.info("New next action : " + nextAction.toString());
        return nextAction;
    }
}
