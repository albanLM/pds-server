package test;

import data.Location;
import database.Neo4JConnection;
import database.Queries;
import org.neo4j.driver.Driver;

import java.util.ArrayList;

public class testNeo4J {
    public static void main(String[] args){
        //Useless, just to test
        Driver d = Neo4JConnection.getDriver();
        System.out.println(d);


        Queries q = new Queries();

        Location closest = q.getClosestLocation(48, 2);
        System.out.println("Closest location:");
        System.out.println(closest);

        ArrayList<Location> terminals = q.getTerminalLocations();
        System.out.println("Terminal locations: "+terminals.size());
        System.out.println(terminals);

        int error = q.createShortestPath(closest, terminals.get(1).getId(), false);
        if(error != 1){
            System.out.println("ERROR");
        }
        System.out.println("Shortest path: ");
        System.out.println("Sizes links//locations: "+q.pathLinks.size()+" // "+q.pathLocations.size());
        System.out.println(q.pathLinks);
        System.out.println(q.pathLocations);

        error = q.createShortestPath(closest, terminals.get(1).getId(), true);
        if(error != 1){
            System.out.println("ERROR");
        }
        System.out.println("Shortest path PMR: ");
        System.out.println("Sizes links//locations: "+q.pathLinks.size()+" // "+q.pathLocations.size());
        System.out.println(q.pathLinks);
        System.out.println(q.pathLocations);

        d.close();
    }
}