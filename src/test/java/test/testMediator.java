package test;

import process.Mediator;

public class testMediator {
    private static Mediator m = new Mediator();//Only one time!

    public static void main(String[] args) {
        String terminals = m.getAllTerminalLocations();
        System.out.println(terminals);

        String jsonSN = "{\n" +
                "    \"initial_position\": [\n" +
                "        42.154,\n" +
                "        58.648\n" +
                "    ],\n" +
                "    \"initial_floor\": 2,\n" +
                "    \"destination\": 10,\n" +
                "    \"isReducedMobilityFriendly\": false,\n" +
                "}";
        m.startNavigation(jsonSN);

        /* INFINITE WHILE
        String jsonNL = "";
        while(jsonNL != null) {
            String currentJson = "{\n" +
                                "    \"vector\": [\n" +
                                "        0.25,\n" +
                                "        0.65,\n" +
                                "        1.21\n" +
                                "    ]\n" +
                                "}";

            jsonNL = m.getNextLocation(currentJson);
            System.out.println(jsonNL);
        }*/
        System.out.println("No more location");

        m.closeDriver();//DON'T FORGET IT!!!
    }
}