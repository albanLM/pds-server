package test;

import jakarta.websocket.*;
import org.glassfish.tyrus.client.ClientManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

@ClientEndpoint
public class testWebSocket {
    private static CountDownLatch latch;
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private final String message = "{\n" +
            "    \"initial_position\": [\n" +
            "        42.154,\n" +
            "        58.648\n" +
            "    ],\n" +
            "    \"initial_floor\": 2,\n" +
            "    \"destination\": 10,\n" +
            "    \"isReducedMobilityFriendly\": false,\n" +
            "}";


    public static void main(String[] args) {
        latch = new CountDownLatch(1);

        ClientManager client = ClientManager.createClient();
        try {
            client.connectToServer(testWebSocket.class, new URI("ws://localhost:8025/navigation/"));
            latch.await();
        } catch (URISyntaxException | InterruptedException e) {
            throw new RuntimeException(e);
        } catch (jakarta.websocket.DeploymentException | IOException e) {
            e.printStackTrace();
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        logger.info("Connected ... " + session.getId());
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @OnMessage
    public String onMessage(String message, Session session) {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            logger.info("Received ...." + message);
            String userInput = bufferRead.readLine();
            return userInput;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info(String.format("Session %s close because of %s", session.getId(), closeReason));
        latch.countDown();
    }
}