package test;

import data.NextDirection;
import data.SensorsVectors;
import data.StartNavigation;

public class testDatas {
    public static void main(String[] args) {
        NextDirection nd = new NextDirection(130.25, 13.14);
        String jsonND = nd.toJson();
        System.out.println(nd);
        System.out.println(jsonND);

        SensorsVectors sv = new SensorsVectors();
        String jsonSV = "{\n" +
                        "    \"vector\": [\n" +
                        "        0.154,\n" +
                        "        0.648,\n" +
                        "        1.151\n" +
                        "    ]\n" +
                        "}";
        System.out.println(jsonSV);
        sv.fromJson(jsonSV);
        System.out.println(sv);

        StartNavigation sn = new StartNavigation();
        String jsonSN = "{\n" +
                        "    \"initial_position\": [\n" +
                        "        42.154,\n" +
                        "        58.648\n" +
                        "    ],\n" +
                        "    \"initial_floor\": 2,\n" +
                        "    \"destination\": 10,\n" +
                        "    \"isReducedMobilityFriendly\": true,\n" +
                        "}";
        System.out.println(jsonSN);
        sn.fromJson(jsonSN);
        System.out.println(sn);
    }
}